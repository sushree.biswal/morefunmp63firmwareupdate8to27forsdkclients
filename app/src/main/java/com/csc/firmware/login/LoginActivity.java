package com.csc.firmware.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.csc.firmware.BluetoothConnectActivity;
import com.csc.firmware.Constant;
import com.csc.firmware.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity {
    TextInputEditText userET, passwordET;
    Button submit;
    String userName, password;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userET = findViewById(R.id.user_name);
        passwordET = findViewById(R.id.password);
        submit = findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName = userET.getText().toString().trim();
                password = passwordET.getText().toString().trim();

                if (userName.length() != 0) {

                    if (password.length() != 0) {
                        dialog = new ProgressDialog(LoginActivity.this);
                        dialog.setMessage("Loading...");
                        dialog.setCancelable(false);
                        dialog.show();
                        loginToken();

                    } else {
                        Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_LONG).show();

                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Please enter user name", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void loginToken() {
        String url = "https://itpl.iserveu.tech/generate/v1";
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String hello = response.getString("hello");
                            byte[] data = Base64.decode(hello, Base64.DEFAULT);
                            String base64 = "";
                            try {
                                base64 = new String(data, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            //base64 = "https://coreuat-zwqcqy3qmq-el.a.run.app/getlogintoken.json";
                            generateToken(base64);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                    }
                });
    }

    private void generateToken(String url) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", userName);
            obj.put("password", password);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                dialog.dismiss();
                                String token = response.getString("token");
                                Constant.token = token;
                                Constant.user_id = userName;
                                String adminName = response.getString("adminName");
                                Intent intent=new Intent(LoginActivity.this, BluetoothConnectActivity.class);
                                startActivity(intent);
//                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                                startActivity(intent);
//                                finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            dialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Incorrect user name and password", Toast.LENGTH_LONG).show();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            dialog.dismiss();
        }

    }


}

