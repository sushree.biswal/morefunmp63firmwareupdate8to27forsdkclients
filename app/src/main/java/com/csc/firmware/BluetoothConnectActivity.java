package com.csc.firmware;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.csc.firmware.adapter.BluetoothItem;
import com.csc.firmware.adapter.BluetoothListAdapter;
import com.csc.firmware.custom_progressbar.FadingCircle;
import com.csc.firmware.custom_progressbar.SpinKitView;
import com.csc.firmware.custom_progressbar.Sprite;
import com.csc.firmware.util.BytesUtil;
import com.csc.firmware.util.RSAUtil;
import com.csc.firmware.util.SweetDialogUtils;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.firestore.CollectionReference;
//import com.google.firebase.firestore.DocumentReference;
//import com.google.firebase.firestore.DocumentSnapshot;
//import com.google.firebase.firestore.EventListener;
//import com.google.firebase.firestore.FirebaseFirestore;
//import com.google.firebase.firestore.FirebaseFirestoreException;
//import com.google.firebase.firestore.QuerySnapshot;
import com.iserveu.inappdistribution.ISUInAppUpdateSDK;
import com.mf.mpos.pub.CommEnum;
import com.mf.mpos.pub.Controler;
import com.mf.mpos.pub.IUpdatePosProc;
import com.mf.mpos.pub.result.ConnectPosResult;
import com.mf.mpos.pub.result.ICAidResult;
import com.mf.mpos.pub.result.ICPublicKeyResult;
import com.mf.mpos.pub.result.LoadMainKeyResult;
import com.mf.mpos.pub.result.LoadWorkKeyResult;
import com.mf.mpos.pub.result.ReadPosInfoResult;
import com.mf.mpos.pub.result.UpdatePosResult;
import com.mf.mpos.util.Misc;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class BluetoothConnectActivity extends AppCompatActivity {

    @BindView(R.id.listview)
    ListView mListView;

    @BindView(R.id.tv_mac)
    TextView tv_mac;

    @BindView(R.id.tv_name)
    TextView tv_name;

    @BindView(R.id.ll_bluetoothDevice)
    LinearLayout ll_bluetoothDevice;

    @BindView(R.id.bluetooth_toolbar)
    Toolbar toolbar;

    private BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothListAdapter mAdapter;
    private BluetoothReceiver bluetoothReceiver;
    private String bluetoothMac = "";
    private String bluetoothName = "";

    PreferenceUtility prefUtlity;
    SharedPreferences sharedPreferences;

    private SweetAlertDialog progressDialog;
    //private FirebaseFirestore firebaseFirestore;

    private String sig = "";
    private String bin = "";
    private String version = "";
    private String deviceVersion = "";

    private String deviceNamee, devceMacc;
    private String[] capks;

    private static String[] premission = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private ReadPosInfoResult readPosInfoResult;
    private Dialog dialog;
    private TextView percentage;

    private String getTransactionType = "";
    private String getTransactionAmount = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_bluetooth);
        permissionRequest(this, 1);
        ISUInAppUpdateSDK.forceUpdateActivity = this.getLocalClassName();


        ButterKnife.bind(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BluetoothConnectActivity.this.onBackPressed();
            }
        });

//        if (getIntent() != null && getIntent().hasExtra("type")) {
//            getTransactionType = getIntent().getStringExtra("type");
//        }
//        if (getIntent() != null && getIntent().hasExtra("amount")) {
//            getTransactionAmount = getIntent().getStringExtra("amount");
//        }

        if (Constant.applicationType.equalsIgnoreCase("CORE")) {

        } else {
            Constant.user_id = Constant.loginID;
            getUserAuthToken();

        }
        mAdapter = new BluetoothListAdapter(this);
        mListView.setAdapter(mAdapter);
        mListView.setFastScrollEnabled(true);
        mListView.setOnItemClickListener(mListClickListener);

        //firebaseFirestore = FirebaseFirestore.getInstance();
        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        prefUtlity = new PreferenceUtility(BluetoothConnectActivity.this);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        Controler.Init(BluetoothConnectActivity.this, CommEnum.CONNECTMODE.BLUETOOTH, 16);
        setManufacturerID(16);
        Controler.SetManufacturerID(16);
        btAdapter.enable();

        if (!getBluetoothMac().isEmpty()) {
            ll_bluetoothDevice.setVisibility(View.VISIBLE);
            tv_name.setText(getBluetoothName());
            tv_mac.setText(getBluetoothMac());
            tv_name.setVisibility(View.VISIBLE);
            tv_mac.setVisibility(View.VISIBLE);
        } else {
            ll_bluetoothDevice.setVisibility(View.INVISIBLE);
        }
        registerReceiver();
    }

    @OnClick({R.id.removeBond, R.id.search, R.id.connect, R.id.isConnect, R.id.disconnect})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.removeBond:
                removeBondMethods();
                break;
            case R.id.search:
                btAdapter.cancelDiscovery();
                startDiscovery();
                break;
            case R.id.connect:
                connectDevice();
                break;
            case R.id.isConnect:
                if (Controler.posConnected()) {
                    Toast.makeText(BluetoothConnectActivity.this, "Device connected", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(BluetoothConnectActivity.this, "Device not connected", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.disconnect:
                Controler.disconnectPos();
                setBlueToothMacAddress("");
                ll_bluetoothDevice.setVisibility(View.INVISIBLE);
                tv_name.setVisibility(View.GONE);
                tv_mac.setVisibility(View.GONE);
                Toast.makeText(BluetoothConnectActivity.this, "Device dis-connected successfully!", Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        btAdapter.cancelDiscovery();
//        unregisterReceiver();
//        finish();

        super.onBackPressed();
        Intent data = new Intent();
        setResult(Activity.RESULT_OK, data);
        super.onBackPressed();
    }

    private AdapterView.OnItemClickListener mListClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int position, long id) {
            mAdapter.setSelected(position);
            connectDevice();
        }
    };

    private void unregisterReceiver() {
        if (bluetoothReceiver != null) {
            try {
                unregisterReceiver(bluetoothReceiver);
            } catch (Exception e) {
            }
        }
        bluetoothReceiver = null;
    }

    private void registerReceiver() {
        try {
            bluetoothReceiver = new BluetoothReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            registerReceiver(bluetoothReceiver, filter);
        } catch (Exception e) {
        }
    }

    private class BluetoothReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (BluetoothDevice.ACTION_FOUND.equals(intent.getAction())) {
                    BluetoothDevice btDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (btDevice != null) {
                        if (btDevice.getName() != null) {
                            mAdapter.addItem(new BluetoothItem(btDevice.getName(), btDevice.getAddress(), false));
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startDiscovery() {
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        mAdapter.clear();
        btAdapter.startDiscovery();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                mAdapter.addItem(new BluetoothItem(device.getName(), device.getAddress(), false));
            }
            mAdapter.notifyDataSetChanged();
            btAdapter.startDiscovery();
        }
    }

    private void connectDevice() {
        SweetDialogUtils.showProgress(BluetoothConnectActivity.this, "Connecting Device", false);
        if (mAdapter.getSelected() >= 0) {
            BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothManager.getAdapter().cancelDiscovery();
            bluetoothMac = ((BluetoothItem) mAdapter.getItem(mAdapter.getSelected())).getAddress();
            bluetoothName = ((BluetoothItem) mAdapter.getItem(mAdapter.getSelected())).getName();
        } else {
            bluetoothMac = getBluetoothMac();
            bluetoothName = getBluetoothName();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Controler.posConnected()) {
                    Controler.disconnectPos();
                }
                try {
                    ConnectPosResult ret = Controler.connectPos(bluetoothMac);
                    if (ret.bConnected) {
                        setBlueToothMacAddress(bluetoothMac);
                        setBlueToothNameAddress(bluetoothName);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv_name.setText(getBluetoothName());
                                tv_mac.setText(getBluetoothMac());
                                ll_bluetoothDevice.setVisibility(View.VISIBLE);
                                tv_name.setVisibility(View.VISIBLE);
                                tv_mac.setVisibility(View.VISIBLE);
                            }
                        });
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Device Connected Successfully");
//                        encryptByPrivateKey();
                        readPosInfoResult = Controler.ReadPosInfo2();
                        deviceVersion = readPosInfoResult.posVer;
                        //getDeviceVersionFromFirestore();


                        if (deviceVersion.contains("8")){
                            sig = "https://firebasestorage.googleapis.com/v0/b/iserveumainapp/o/morefun_paynext_27%2Fdev.bin.sig?alt=media&token=363111f9-7e03-4800-8c95-424c5f517008";
                            bin = "https://firebasestorage.googleapis.com/v0/b/iserveumainapp/o/morefun_paynext_27%2Fdev.bin?alt=media&token=4fac0341-17ee-4366-8dd5-9b0049e721c8";

                            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                            new DownloadSigVersion(BluetoothConnectActivity.this, sig, bin).execute();

                        } else if (deviceVersion.contains("27")){
                           // Toast.makeText(BluetoothConnectActivity.this, "NO need to Update", Toast.LENGTH_SHORT).show();
                            SweetDialogUtils.showMsg(BluetoothConnectActivity.this,"No need to update");
                            deleteApp();
                        }
//                          else{
//                           SweetDialogUtils.showMsg(BluetoothConnectActivity.this,"Update not possible");
//                          // deleteApp();
//                         }

//                        if (getTerminalUpdateStatus()) {
//                            encryptByPrivateKey();
//                        } else {
//                            updateTerminalOta();//flow reverse due to production
//                        }
                        Log.d("This", "start injection");
                    } else {
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Device not connected");
                    }
                } catch (Exception e) {
                    SweetDialogUtils.changeAlertType(BluetoothConnectActivity.this, "Device connected successfully", SweetAlertDialog.SUCCESS_TYPE);
                }
            }
        }).start();
    }

    private void encryptByPrivateKey() {
        SweetDialogUtils.showProgress(BluetoothConnectActivity.this, "Please wait...", false);
        try {
            PrivateKey privateKey = RSAUtil.loadPrivateKey(getAssets().open("privatekey.pem"));
            String etPlainText = "969b5b7084e1cc34a24add51cacd8e93bc23950235c5b908edfe88c398048c6c";
            mSignData = RSAUtil.encrypt(BytesUtil.hexString2ByteArray(etPlainText), privateKey);
            Log.d("VendorVerifyActivity", "mSignData:" + BytesUtil.bytes2Hex(mSignData));
            etSignData = BytesUtil.bytes2Hex(mSignData);
            signTransactionKey(etPlainText, etSignData);
        } catch (Exception e) {
            Toast.makeText(BluetoothConnectActivity.this, "Load private key failed", Toast.LENGTH_SHORT).show();
            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            Log.d("VendorVerifyActivity", "Illegal private key");
            e.printStackTrace();
        }
    }

    private void signTransactionKey(String plainText, String signData) {
        if (Controler.posConnected()) {
            runOnThread(new Runnable() {
                @Override
                public void run() {
                    boolean ret = Controler.vendorVerify(BytesUtil.hexString2ByteArray(plainText),
                            BytesUtil.hexString2ByteArray(signData));
                    if (ret) {
                        // SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        //SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.operation_success));
                        getDeviceInfo();
                        //pairSuccessResponse(getModifiedSerialNumber(readPosInfoResult.sn), bluetoothMac);
                        Log.d("BluetoothConnect", "start injection");
                    } else {
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.operation_fail));
                    }
                }
            });
        } else {
            SweetDialogUtils.showError(BluetoothConnectActivity.this, getString(R.string.device_not_connect));
            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            //SweetDialogUtils.showMsg(BluetoothConnectActivity.this,getString(R.string.device_not_connect));
        }
    }

    /**
     * get device info
     */
    private void getDeviceInfo() {
        //SweetDialogUtils.showProgress(BluetoothConnectActivity.this,"Please wait...",false);
        if (Controler.posConnected()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getDeviceDetailsInfo();
                }
            });
        } else {
            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.device_not_connect));
        }
    }

    /**
     * get device details info
     */
    private void getDeviceDetailsInfo() {
        readPosInfoResult = Controler.ReadPosInfo2();

        StringBuilder builder = new StringBuilder();
        builder.append("\nPosVer:" + readPosInfoResult.posVer);
        builder.append("\nDataVer:" + readPosInfoResult.dataVer);
        builder.append("\nModel:" + readPosInfoResult.model);
        builder.append("\nSn:" + readPosInfoResult.sn);
        builder.append("\nStatus:" + readPosInfoResult.initStatus);
        /**
         * Battery status
         * 0    No power off
         * 1    Low battery (tension is low)
         * 2    On battery (normal)
         * 3    High battery (full battery)
         * 4    Full
         * 5    Charging
         */
        builder.append("\nBattery:" + readPosInfoResult.btype);
        Log.d("Device info====", builder.toString());
        Constant.DEVICE_SERIAL_NUMBER = readPosInfoResult.sn;
        deviceVersion = readPosInfoResult.posVer;
        //check device mapping
        //checkDeviceStatus(getModifiedSerialNumber(readPosInfoResult.sn), bluetoothMac);
        pairSuccessResponse(getModifiedSerialNumber(readPosInfoResult.sn), bluetoothMac);
    }

    public void checkDeviceStatus(final String serialNo, final String deviceMac) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("deviceSlNo", serialNo.replace("D180-", "").trim());
            jsonObject.put("userName", Constant.user_id);

            Log.e(TAG, "checkDeviceStatus: calling");

            AndroidNetworking.post("https://us-central1-creditapp-29bf2.cloudfunctions.net/isuApi/matmmapping/mapwithusername")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG, "checkDeviceStatus onResponse: " + response);
                            try {
                                JSONObject responseData = new JSONObject(response.toString());
                                String status = responseData.getString("status");
                                if (status.equalsIgnoreCase("0")) {

                                    // pairing pax with blueetooth........
                                    prefUtlity.saveString("DEVICE_NAME", serialNo.replace("D180-", "").trim());
                                    prefUtlity.saveString("DEVICE_MAC", deviceMac.trim());
                                    //getDeviceVersionFromFirestore(currentName,currentMac);
                                    pairSuccessResponse(serialNo, deviceMac);
                                } else {
                                    Controler.disconnectPos();
                                    String description = responseData.getString("desc");
                                    SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                                    showAlert(description + "\nplease press OK to proceed further.");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                                showAlert("Unauthorized device paired please call or email helpdesk to continue ATM transactions.");
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                            SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Please try again");
                            String errorStr = anError.getErrorBody();
                            try {
                                JSONObject obj = new JSONObject(errorStr);
                                String status = obj.getString("status");
                                if (status.equalsIgnoreCase("1")) {
                                    String sttsDesc = obj.getString("errorMessage");
                                    if (sttsDesc.equalsIgnoreCase("user name can't be null")) {
                                        showAlert(sttsDesc);

                                    } else {
                                        showAlert(sttsDesc);
                                    }

                                } else {
                                    Toast.makeText(BluetoothConnectActivity.this, "Oops!! Server error.", Toast.LENGTH_LONG).show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void pairSuccessResponse(String deviceSlNo, String deviceMac) {

        String devName = "";
        devName = prefUtlity.getString("DEVICE_NAME", "");
        if (!deviceSlNo.equalsIgnoreCase("")) {
            if (devName.equalsIgnoreCase(deviceSlNo.replace("D180-", "").trim())) {
                deviceNamee = prefUtlity.getString("DEVICE_NAME", null);
                devceMacc = prefUtlity.getString("DEVICE_MAC", null);
                if (getInjectKeyStatus()) {
                    deviceRegisteration(deviceNamee, devceMacc);
                } else {
                    //getTmkKeyFromFireStore();////due to production
                }
            } else {
                checkDeviceStatus(deviceSlNo, deviceMac);
            }
        } else {

            deviceNamee = prefUtlity.getString("DEVICE_NAME", null);
            devceMacc = prefUtlity.getString("DEVICE_MAC", null);
            if (getInjectKeyStatus()) {
                deviceRegisteration(deviceNamee, devceMacc);
            } else {
                //getTmkKeyFromFireStore();////due to production
            }

        }
    }


    public void deviceRegisteration(final String deviceSlNo, final String deviceMac) {
        AndroidNetworking.get("https://getnptimestamp.iserveu.website/getTimestamp")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "deviceRegisteration onResponse: " + response);
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            if (status.equalsIgnoreCase("0")) {
                                String milisec = obj.getString("milisecond");

                                long current_milisec = 0;
                                if (!milisec.equalsIgnoreCase("")) {
                                    current_milisec = Long.parseLong(milisec);
                                }
                                long injectTime = prefUtlity.getLong("INJECTED_TIME", 0);

                                if (injectTime > 0) {
                                    String device_name = prefUtlity.getString("DEVICE_NAME", null);
                                    long saved_time = prefUtlity.getLong("INJECTED_TIME", 0);
                                    Calendar saved_calendar = Calendar.getInstance();
                                    saved_calendar.setTimeInMillis(saved_time);

                                    long diff = current_milisec - saved_calendar.getTimeInMillis();

                                    float dayCount = (float) diff / (24 * 60 * 60 * 1000);
                                    if (device_name.equalsIgnoreCase(deviceSlNo.replace("D180-", ""))) {

                                        getTpkTdkKey(deviceSlNo, deviceMac, current_milisec);
                                    } else {
                                        getTpkTdkKey(deviceSlNo, deviceMac, current_milisec);
                                    }

                                } else {
                                    getTpkTdkKey(deviceSlNo, deviceMac, current_milisec);
                                }
                            } else {
                                SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                                Toast.makeText(BluetoothConnectActivity.this, "Time stamp not generated .", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                            Toast.makeText(BluetoothConnectActivity.this, "Time stamp not generated .", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        // dialogDismiss();
                        Toast.makeText(BluetoothConnectActivity.this, "Server Error. Time stamp not generated .", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    /**
     * get master key from server
     */

    public void getTpkTdkKey(final String deviceSerialNo, final String deviceMac, final long time) {
        //String Url = "https://matm.iserveu.tech/LiveTesting/" + "generateTPKandTDK/" + currentName;
        String url="";
        if(Constant.applicationType.equalsIgnoreCase("CORE")){  //For app
             url = Constant.BASE_URL + "/generateTPKandTDK/" + deviceSerialNo;
        }
        else {
            //For SDK user
            url = Constant.BASE_URL+"/generateTPKandTDK/"+deviceSerialNo+"/"+ Constant.user_id;

        }

        Log.e(TAG, "injectKeys: url " + url);
        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", Constant.token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            if (response != null) {
                                JSONObject jObj = new JSONObject(response.toString());
                                String jsonSts = jObj.getString("status");
                                if (jsonSts.equalsIgnoreCase("-1")) {
                                    String stsDesc = jObj.getString("statusDesc");
                                    showAlert(stsDesc);
                                } else {
                                    //saveDeviceInfoForMapping(currentName, currentMac);
                                    String tpkString = jObj.getString("tpk");
                                    String tdkString = jObj.getString("tdk");
                                    String tpkKcv = jObj.getString("tpkKcv");
                                    String tdkKcv = jObj.getString("tdkKcv");
                                    if (!tpkString.isEmpty() && !tdkString.isEmpty()
                                            && !tpkKcv.isEmpty() && !tdkKcv.isEmpty()) {
                                        uploadWorkerKey(tpkString, tdkString, tpkKcv, tdkKcv);
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Please try again");
                        //progressDialog.dismiss();
                        //  hideLoader();
                        Log.e(TAG, "onError: " + anError.getErrorBody());
                        String response = anError.getErrorBody();
                        //{"status":-1,"statusDesc":"Device is not registered with us."}
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equalsIgnoreCase("-1")) {
                                String desc = jsonObject.getString("statusDesc");
                                // showUserOnboardStatusFailure(desc);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void uploadWorkerKey(final String tpk, final String tdk, final String tpkKcv, final String tdkKcv) {
        if (Controler.posConnected()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateWorkingKey(tpk, tdk, tpkKcv, tdkKcv);
                }
            });
        } else {
            //SweetDialogUtils.showError(BluetoothConnectActivity.this, getString(R.string.device_not_connect));
            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.device_not_connect));
        }
    }


    private void updateWorkingKey(String tpk, String tdk, String tpkKcv, String tdkKcv) {
        String pinKey = tpk;
        String macKey = tpk;
        String tdkKey = tdk;
        String pinKvc = tpkKcv + "00";
        String macKvc = tpkKcv + "00";
        String tdkKvc = tdkKcv + "00";

        Log.d("TPK Key====", pinKey);
        Log.d("Mac Key====", macKey);
        Log.d("TDK Key====", tdkKey);
        Log.d("TPK Kcv====", pinKvc);
        Log.d("TDK Kcv====", tdkKvc);
        Log.d("MAC Kcv====", macKvc);

        if (pinKey == null || pinKey.length() != 32) {
            return;
        }

        if (macKey == null || macKey.length() != 32) {
            return;
        }

        if (tdkKey == null || tdkKey.length() != 32) {
            return;
        }

        if (pinKvc == null || pinKvc.length() != 8) {
            return;
        }

        if (macKvc == null || macKvc.length() != 8) {
            return;
        }

        if (tdkKvc == null || tdkKvc.length() != 8) {
            return;
        }

        String key = pinKey + pinKvc + macKey + macKvc + tdkKey + tdkKvc;

        byte[] keyArrays = Misc.asc2hex(key);
        Log.d("ChooseCardActivity", "updateWorkingKey key:" + key);

        LoadWorkKeyResult result = Controler.LoadWorkKey(getKeyIndex(), CommEnum.WORKKEYTYPE.DOUBLEMAG, keyArrays, keyArrays.length);
        if (result.loadResult) {
            //SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            //startCard read
            showKeyInjectSuccess();
        } else {
            //SweetDialogUtils.showError(BluetoothConnectActivity.this, "Key injection failed!");
            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Key injection failed!");
        }
    }


    private void showKeyInjectSuccess() {
        //SweetDialogUtils.showSuccess(BluetoothConnectActivity.this,"Inject key success");
        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Inject key success");
        deleteApp();

//        if (Controler.posConnected()) {
//            Intent intent = new Intent(BluetoothConnectActivity.this, ChooseCardActivity.class);
//            intent.putExtra("type",getTransactionType);
//            intent.putExtra("amount",getTransactionAmount);
//            startActivity(intent);
//        }

    }



    private CommEnum.MAINKEYENCRYPT getMainKeyEncryptType() {
        return CommEnum.MAINKEYENCRYPT.PLAINTEXT;
    }

    private CommEnum.KEYINDEX getKeyIndex() {
        try {
            int index = Integer.parseInt("0");
            return CommEnum.KEYINDEX.values()[index];
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return CommEnum.KEYINDEX.INDEX0;
    }


    private void removeBondMethods() {
        Controler.disconnectPos();
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        for (BluetoothDevice b : pairedDevices) {
            removeBondMethod(b);
        }
        mAdapter.clear();
        mAdapter.notifyDataSetChanged();
    }

    private int removeBondMethod(BluetoothDevice btDev) {
        //Using reflection method calls BluetoothDevice.createBond(BluetoothDevice remoteDevice);
        Method removeBondMethod = null;
        try {
            removeBondMethod = BluetoothDevice.class.getMethod("removeBond");
            removeBondMethod.invoke(btDev);
            Log.w("removeBondMethod", "removeBondMethod  	removeBondMethod.invoke(btDev); ");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    public void setBlueToothMacAddress(String mac) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("BluetoothMac", mac);
        editor.apply();
    }

    public void setBlueToothNameAddress(String name) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("BluetoothName", name);
        editor.apply();
    }

    public String getBluetoothMac() {
        return sharedPreferences.getString("BluetoothMac", "");
    }

    public String getBluetoothName() {
        return sharedPreferences.getString("BluetoothName", "");
    }


    public void setManufacturerID(int id) {
        SharedPreferences.Editor mEditor = sharedPreferences.edit();
        mEditor.putInt("manufacturerID", id);
        mEditor.apply();
    }

    public void setDeviceSlNo(String deviceSlNo) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("deviceslno", deviceSlNo);
        editor.apply();
    }

    public String getDeviceSlNo() {
        return sharedPreferences.getString("deviceslno", "");
    }

    public void setInjectStatus(boolean status) {
        Constant.INJECT_TIME_KEYSTATUS = sharedPreferences.edit().putBoolean("injectkeystatus", status).commit();
    }

    public boolean getInjectKeyStatus() {
        return sharedPreferences.getBoolean("injectkeystatus", false);
    }

    public void setTerminalUpdateStatus(boolean updateStatus) {
       Constant.TERMINAL_UPDATE_KEYSTATUS = sharedPreferences.edit().putBoolean("updateterminalstatus", updateStatus).commit();
    }

    public boolean getTerminalUpdateStatus() {
        return sharedPreferences.getBoolean("updateterminalstatus", false);
    }

    public void setDeviceSlNumber(boolean slno) {
        Constant.DEVICE_SERIAL_NUMBER_ = sharedPreferences.edit().putBoolean("deviceno", slno).commit();
    }

    public boolean getDeviceSlNumber() {
        return sharedPreferences.getBoolean("deviceno", false);
    }

    private String etSignData;
    private byte[] mSignData;


    protected void runOnThread(Runnable runnable) {
        new Thread(runnable).start();
    }


    /**
     * show progress loader
     */
    private void showLoader() {
        progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDialog.setTitleText("Please wait, loading master key...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /**
     * hide progress loader
     */
    private void hideLoader() {
        if (progressDialog != null)
            progressDialog.cancel();
    }

    /**
     * insert tmk
     */

    /**
     * load TMK from firestore
     */
/*
    private void getTmkKeyFromFireStore() {
        CollectionReference keyTmk = firebaseFirestore.collection("morefun_paynext");
        keyTmk.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if (error != null) {
                    Log.e(TAG, "onEvent: error occured");
                }

                if (value.isEmpty()) {
                    Log.e(TAG, "onEvent: empty snap");
                } else {
                    List<DocumentSnapshot> doc = value.getDocuments();
                    if (doc.size() > 0) {
                        //get 3rd index value for tmk
                        Map<String, Object> tmkDataMap = doc.get(3).getData();
                        String kcv = (String) tmkDataMap.get("kcv");
                        String tmk = (String) tmkDataMap.get("tmk");
                        if (tmk != null && !tmk.isEmpty() && kcv != null && !kcv.isEmpty()) {
                            if (!Controler.posConnected()) {
//                                hideLoader();
                                SweetDialogUtils.showError(BluetoothConnectActivity.this, getString(R.string.device_not_connect));
                                SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                                return;
                            }
                            //SweetDialogUtils.showProgress(BluetoothConnectActivity.this, getString(R.string.download_tmk), false);
                            runOnThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadTerminalMasterKey(tmk, kcv);
                                }
                            });

                        }
                    } else {
                        //SweetDialogUtils.showError(BluetoothConnectActivity.this,"Master Key load fail!");
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Master Key load fail!");
                    }
                }
            }
        });
    }
*/

    /**
     * inject TMK
     */
    private void loadTerminalMasterKey(String tmk, String kcv) {
        if (tmk == null || tmk.length() != 32) {
            return;
        }

        if (kcv == null || kcv.length() != 8) {
            return;
        }
        byte[] keyBuf = BytesUtil.hexString2ByteArray(tmk);

        byte[] kekD1 = BytesUtil.subBytes(keyBuf, 0, 8);
        byte[] kekD2 = BytesUtil.subBytes(keyBuf, 8, 8);
        byte[] kvcBuf = BytesUtil.hexString2ByteArray(kcv);

        Misc.traceHex(TAG, "updateMainKey kekD1", kekD1);
        Misc.traceHex(TAG, "updateMainKey kekD2", kekD2);
        Misc.traceHex(TAG, "updateMainKey kvc", kvcBuf);
        LoadMainKeyResult result = Controler.LoadMainKey(getMainKeyEncryptType(),
                getKeyIndex(),
                CommEnum.MAINKEYTYPE.DOUBLE,
                kekD1, kekD2, kvcBuf);

        showResult(result.loadResult);
    }

    private void showResult(final boolean result) {
        Log.d("TMK Status", "" + result);
        if (result) {
            //SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            //SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "TMK injected successfully");

            //start aid download and injection
            //getAidKeyFromFireStore();
        } else {
            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.download_master_key_fail));

        }
    }

    /**
     * download AID list from fire store.
     */
/*
    private void getAidKeyFromFireStore() {
        // showLoader();
        //SweetDialogUtils.showProgress(BluetoothConnectActivity.this,"Please wait...",false);
        CollectionReference keyAid = firebaseFirestore.collection("morefun_paynext");
        keyAid.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if (error != null) {
                    Log.e(TAG, "onEvent: error occured");
                }
                if (value.isEmpty()) {
                    Log.e(TAG, "onEvent: empty snap");
                } else {
                    List<DocumentSnapshot> doc = value.getDocuments();
                    if (doc.size() > 0) {
                        //get 0 index value for aid
                        Map<String, Object> aidDataMap = doc.get(0).getData();
                        String[] aids = new String[aidDataMap.size()];
                        ArrayList<String> keyList = new ArrayList<>(aidDataMap.keySet());
                        for (int i = 0; i < keyList.size(); i++) {
                            aids[i] = (String) aidDataMap.get(keyList.get(i));
                        }
                        if (!Controler.posConnected()) {
//                            hideLoader();
                            SweetDialogUtils.showError(BluetoothConnectActivity.this, getString(R.string.device_not_connect));
                            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                            return;
                        }
                        //SweetDialogUtils.showProgress(BluetoothConnectActivity.this, getString(R.string.download_aid), false);
                        runOnThread(new Runnable() {
                            @Override
                            public void run() {
                                downloadAID(aids);
                            }
                        });
                    } else {
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "AID Key load fail!");
                    }
                }
            }
        });
    }
*/

    /**
     *
     */
    private void downloadAID(String[] aids) {
        for (int j = 0; j < aids.length; j++) {
            String aid = aids[j];
            //setProgress("Start download AID(" + (j + 1) + "/" + aids.length + ")");
            Log.d("j value===", "" + (j + 1));
            ICAidResult result = Controler.ICAidManage(CommEnum.ICAIDACTION.ADD, Misc.asc2hex(aid));
            if (!result.commResult.equals(CommEnum.COMMRET.NOERROR)) {
                SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                SweetDialogUtils.changeAlertType(BluetoothConnectActivity.this, getString(R.string.aid_operation_fail), SweetAlertDialog.ERROR_TYPE);
                return;
            }
        }
        //after successfully inject aids starts capks
        //SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
        //SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.download_aid_success));
        //getCapKeyFromFireStore();
    }

    /**
     * start capk download from firestore
     */
/*
    private void getCapKeyFromFireStore() {
        CollectionReference keyAid = firebaseFirestore.collection("morefun_paynext");
        keyAid.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if (error != null) {
                    Log.e(TAG, "onEvent: error occured");
                }
                if (value.isEmpty()) {
                    Log.e(TAG, "onEvent: empty snap");
                } else {
                    List<DocumentSnapshot> doc = value.getDocuments();
                    if (doc.size() > 0) {
                        //get 1 index value for capk
                        Map<String, Object> capkDataMap = doc.get(1).getData();
                        capks = new String[capkDataMap.size()];
                        ArrayList<String> keyList = new ArrayList<>(capkDataMap.keySet());
                        for (int i = 0; i < keyList.size(); i++) {
                            capks[i] = (String) capkDataMap.get(keyList.get(i));
                        }
                        if (!Controler.posConnected()) {
//                            hideLoader();
                            SweetDialogUtils.showError(BluetoothConnectActivity.this, getString(R.string.device_not_connect));
                            return;
                        }
                        //SweetDialogUtils.showProgress(BluetoothConnectActivity.this, getString(R.string.download_aid), false);
                        runOnThread(new Runnable() {
                            @Override
                            public void run() {
                                clearCapk();
                            }
                        });
                    } else {
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "CAPK Key load fail!");
                    }
                }
            }
        });
    }
*/

    private void clearCapk() {
        ICPublicKeyResult result = Controler.ICPublicKeyManage(CommEnum.ICPUBLICKEYACTION.CLEAR, null);
        if (result.commResult.equals(CommEnum.COMMRET.NOERROR)) {
            downloadCapk(capks);
        } else {
            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Capk failed");
        }
    }

    /**
     * download capks
     *
     * @param rids
     */
    private void downloadCapk(String[] rids) {
        for (int j = 0; j < rids.length; j++) {
            String rid = rids[j];
            //setProgress("Downlaod Capk(" + (j + 1) + "/" + rids.length + ")");
            ICPublicKeyResult result = Controler.ICPublicKeyManage(CommEnum.ICPUBLICKEYACTION.ADD, Misc.asc2hex(rid));
            Log.d("j value======", "" + (j + 1));
            if (!result.commResult.equals(CommEnum.COMMRET.NOERROR)) {
                SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                SweetDialogUtils.changeAlertType(BluetoothConnectActivity.this, "Capk failed", SweetAlertDialog.ERROR_TYPE);
                return;
            }
        }
        //after successfully inject capks starts emv.
        //SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
        //SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.download_capk_success));
        //getEmvFromFireStore();
    }

    /**
     * get emv from firestore.
     */
/*
    private void getEmvFromFireStore() {
        // SweetDialogUtils.showProgress(BluetoothConnectActivity.this,"Please wait...",false);
        // showLoader();
        CollectionReference keyTmk = firebaseFirestore.collection("morefun_paynext");
        keyTmk.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if (error != null) {
                    Log.e(TAG, "onEvent: error occured");
                }

                if (value.isEmpty()) {
                    Log.e(TAG, "onEvent: empty snap");
                } else {
                    List<DocumentSnapshot> doc = value.getDocuments();
                    if (doc.size() > 0) {
                        //get 2rd index value for tmk
                        Map<String, Object> tmkDataMap = doc.get(2).getData();
                        String emvData = (String) tmkDataMap.get("emvdata");

                        if (emvData != null && !emvData.isEmpty()) {
                            if (!Controler.posConnected()) {
                                SweetDialogUtils.showError(BluetoothConnectActivity.this, getString(R.string.device_not_connect));
                                return;
                            }
                            //SweetDialogUtils.showProgress(BluetoothConnectActivity.this, getString(R.string.load_emv), false);
                            runOnThread(new Runnable() {
                                @Override
                                public void run() {
                                    setEmv(emvData);
                                }
                            });

                        }
                    } else {
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "EMV Key load fail!");
                    }
                }
            }
        });
    }
*/


    /**
     * set emv
     */
    private void setEmv(String emvData) {
        boolean ret = setTlv(emvData);
        if (ret) {
            //SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            //SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.setting_success));
            setInjectStatus(true);
            deviceRegisteration(deviceNamee, devceMacc);
            //updateTerminalOta();
        } else {
            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
            SweetDialogUtils.showMsg(BluetoothConnectActivity.this, getString(R.string.setting_fail));
        }
    }

    private boolean setTlv(String emvParam) {
        return Controler.SetEmvParamTlv(emvParam);
    }

    /**
     * set progress status
     *
     * @param result
     */
    private void setProgress(final String result) {
        SweetDialogUtils.setProgressText(BluetoothConnectActivity.this, result);
    }

    /**
     * update terminal a
     *
     * @param
     */
/*
    public void updateTerminalOta() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = firebaseFirestore.collection("Matm2Config").document("com.morefun.paynext.master.card");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                Log.e("TransactionActivity", "onComplete: executed");
                if (task.isSuccessful()) {
                    if (task.getResult().getData() != null) {
                        Log.e("TransactionActivity", "onComplete: has data status " + task.getResult().getData());
                        Map<String, Object> packageMap = task.getResult().getData();
                        sig = String.valueOf(packageMap.get("sig"));
                        bin = String.valueOf(packageMap.get("bin"));
                        SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                        new DownloadSigVersion(BluetoothConnectActivity.this, sig, bin).execute();
                    }
                }

            }
        });
    }
*/

    public class DownloadSigVersion extends AsyncTask<String, Integer, Boolean> {
        private final String TAG = DownloadSigVersion.class.getSimpleName();
        private Context context;
        private String urlString = "";
        private String binUrl = "";

        public DownloadSigVersion(Context context, String urlString, String binUrl) {
            this.context = context;
            this.urlString = urlString;
            this.binUrl = binUrl;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showUpdateTerminalDialog(BluetoothConnectActivity.this);
            //SweetDialogUtils.showProgress(BluetoothConnectActivity.this, "Getting Your Device Ready...Please do not press Back or Refresh button", false);
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                //SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                //SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Downloaded");
                new DownloadBinVersion(context, binUrl).execute();
            } else {
                SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Error: Try Again");
            }
        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            Boolean flag = false;
            String path;
            try {
                URL url = new URL(urlString);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(false);
                c.connect();
                path = context.getFilesDir().getParentFile().getPath();

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                    path = context.getFilesDir().getParentFile().getPath();
//                } else {
//                    path = context.getFilesDir().getParentFile().getPath();
//                }
                File directory = new File(path);
                if (!directory.exists()) {
                    directory.mkdir();
                }
                File outputFile = new File(directory, "dev.bin.sig");
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();
                int total_size = 5281692;//size of apk
                byte[] buffer = new byte[1024];
                int len1 = 0;
                int per = 0;
                int downloaded = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                    downloaded += len1;
                    per = (int) (downloaded * 100 / total_size);
                    publishProgress(per);
                }
                fos.close();
                is.close();
                flag = true;
                Constant.sigStringPath = path + "/dev.bin.sig";

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                    NetworkConstant.sigStringPath = path + "/dev.bin.sig";
//                } else {
//                    NetworkConstant.sigStringPath = path + "/dev.bin.sig";
//                }
            } catch (Exception e) {
                Log.e(TAG, "Update Error: " + e.getMessage());
                e.printStackTrace();
                flag = false;
            }
            return flag;
        }

    }

    private class DownloadBinVersion extends AsyncTask<String, Integer, Boolean> {
        private final String TAG = DownloadSigVersion.class.getSimpleName();
        private Context context;
        private String urlString = "";

        public DownloadBinVersion(Context context, String urlString) {
            this.context = context;
            this.urlString = urlString;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
        }

        @Override
        protected void onPostExecute(Boolean result) {

            super.onPostExecute(result);

            if (result) {
                //SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                //SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Downloaded Bin");
                //loadSignData();     //no need for production
                terminalUpdate();
            } else {
                //SweetDialogUtils.showSuccess(BluetoothConnectActivity.this,"Error: Try Again");
                SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Error: Try Again");
            }
        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            Boolean flag = false;
            String path;
            try {
                URL url = new URL(urlString);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(false);
                c.connect();
                path = context.getFilesDir().getParentFile().getPath();

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                    path = context.getFilesDir().getParentFile().getPath();
//
//                } else {
//                    path = context.getFilesDir().getParentFile().getPath();
//                }
                File directory = new File(path);
                if (!directory.exists()) {
                    directory.mkdir();
                }
                File outputFile = new File(directory, "dev.bin");
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();
                int total_size = 5281692;//size of apk
                byte[] buffer = new byte[1024];
                int len1 = 0;
                int per = 0;
                int downloaded = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                    downloaded += len1;
                    per = (int) (downloaded * 100 / total_size);
                    publishProgress(per);
                }
                fos.close();
                is.close();
                flag = true;
                Constant.binStringPath = path + "/dev.bin";

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                    NetworkConstant.binStringPath = path + "/dev.bin";
//                } else {
//                    NetworkConstant.binStringPath = path + "/dev.bin";
//                }
            } catch (Exception e) {
                Log.e(TAG, "Update Error: " + e.getMessage());
                e.printStackTrace();
                flag = false;
            }
            return flag;
        }
    }


    /**
     * load sign data
     */
    public void loadSignData() {
        if (!Controler.posConnected()) {
            SweetDialogUtils.showError(BluetoothConnectActivity.this, getString(R.string.connect_device_first));
            return;
        }
        SweetDialogUtils.showProgress(BluetoothConnectActivity.this, "Load Sign Key", false);
        runOnThread(new Runnable() {
            @Override
            public void run() {
                if (Controler.loadVerifySignData(readSignData())) {
                    //SweetDialogUtils.showSuccess(BluetoothConnectActivity.this,"Load Sign Success");
                    SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                    SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Load Sign Success");
                    terminalUpdate();
                } else {
                    //SweetDialogUtils.showError(BluetoothConnectActivity.this, "Load Sign failed");
                    SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                    SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Load Sign failed");
                }
            }
        });
    }

    public byte[] readSignData() {
        try {
            InputStream inputStream;
            File file = new File(Constant.sigStringPath);
            if (!file.exists()) {
                return null;
            }
            Log.d(TAG, "updateFirmware from storage");
            inputStream = new FileInputStream(file);
            return BytesUtil.input2byte(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * update terminal
     */
    private void terminalUpdate() {
        if (!Controler.posConnected()) {
            SweetDialogUtils.showError(BluetoothConnectActivity.this, getString(R.string.connect_device_first));
            return;
        }
        //SweetDialogUtils.showProgress(BluetoothConnectActivity.this, getString(R.string.terminal_update), false);
        runOnThread(new Runnable() {
            @Override
            public void run() {
                if (updateFirmware()) {
                    dialog.dismiss();
                    //SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                    SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Upgrading Device Setup");
                    setTerminalUpdateStatus(true);
                    setDeviceSlNo(Constant.DEVICE_SERIAL_NUMBER);
                    SweetDialogUtils.showProgress(BluetoothConnectActivity.this, "Please wait...", false);
                    new Thread() {
                        public void run() {
                            try {
                                sleep(5000);
                            } catch (Exception e) {
                            }
                            SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                            if (Controler.posConnected()) {
                                ConnectPosResult ret = Controler.connectPos(bluetoothMac);
                                if (ret.bConnected) {
                                    //encryptByPrivateKey();//After terminal update vendor verify
                                    deleteApp();
                                }
                            } else {
                                SweetDialogUtils.showError(BluetoothConnectActivity.this, "Please connect your device");
                            }
                        }
                    }.start();
                } else {
                    Controler.disconnectPos();
                    dialog.dismiss();
                    //SweetDialogUtils.showError(BluetoothConnectActivity.this, "Update failed");
                    SweetDialogUtils.cancelProgress(BluetoothConnectActivity.this);
                    SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Update failed");

                }
            }
        });
    }
    void deleteApp() {
        Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:com.csc.firmware"));
        startActivity(intent);
    }

    public Boolean updateFirmware() {
        try {
            InputStream inputStream;
            File file = new File(Constant.binStringPath);
            if (!file.exists()) {
                return false;
            }
            Log.d(TAG, "updateFirmware from storage");
            inputStream = new FileInputStream(file);

            UpdatePosProc updateProc = new UpdatePosProc(inputStream);
            UpdatePosResult result = Controler.UpdatePos(updateProc);
            if (result.isComplete()) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }

    public class UpdatePosProc implements IUpdatePosProc {
        InputStream fs;

        public UpdatePosProc(InputStream fs) {
            // TODO Auto-generated constructor stub
            this.fs = fs;
        }

        @Override
        public void UpdateProcess(final int totalSize, final int alreadySize) {
            //setProgress("Upgrading..." + alreadySize + "/" + totalSize);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    double getAlreadySize = Double.valueOf(alreadySize);
                    double getTotalSize = Double.valueOf(totalSize);
                    if (getAlreadySize > 100) {
                        double percentageValue = (getAlreadySize / getTotalSize) * 100;
                        int finalPercentageValue = (int) percentageValue;
                        percentage.setText(String.valueOf(finalPercentageValue) + "%");
                    } else if (getAlreadySize == getTotalSize) {
                        percentage.setText(String.valueOf(100) + "%");
                    }
                }
            });
        }

        @Override
        public int totalsize() throws IOException {
            // TODO Auto-generated method stub
            return this.fs.available();
        }

        @Override
        public int read(byte[] buffer, int byteOffset, int byteCount) throws IOException {
            // TODO Auto-generated method stub
            return this.fs.read(buffer, byteOffset, byteCount);
        }
    }

    public static boolean permissionRequest(Activity activity, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int storagePermission = activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int access_location = activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            int read_storage_permission = activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);

            //Check the permission, if don't, you need to apply
            if (storagePermission != PackageManager.PERMISSION_GRANTED ||
                    access_location != PackageManager.PERMISSION_GRANTED ||
                    read_storage_permission != PackageManager.PERMISSION_GRANTED
            ) {
                //apply the permission
                activity.requestPermissions(premission, requestCode);
                //return false。There's no permission
                return false;
            }
        }
        //Already apply the permission
        return true;
    }

    /**
     * get last 8digit number
     */
    private String getModifiedSerialNumber(String serialNumber) {
        String seralNumberModified = serialNumber.substring(6, serialNumber.length());
        return seralNumberModified;
    }

    public void showAlert(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(BluetoothConnectActivity.this);
        builder.setTitle("Alert!!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.show();
    }

/*
    private void getDeviceVersionFromFirestore() {
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        DocumentReference docRef = firebaseFirestore.collection("Matm2Config").document("com.morefun.paynext.master.card");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    if (task.getResult().getData() != null) {
                        Map<String, Object> packageMap = task.getResult().getData();
                        version = String.valueOf(packageMap.get("version"));
                        if (deviceVersion.contains(version)) {
                            //if device version match with what we are getting from firestore then
                            //encryptByPrivateKey();
                            //pairSuccessResponse(currentName, currentMac);
                            Toast.makeText(BluetoothConnectActivity.this, "No need to Update", Toast.LENGTH_SHORT).show();
                            deleteApp();
                            // Log.d("pair:", "pair success");
                        } else {
                            updateTerminalOta();//flow reverse due to production
                            Log.d("update:", "Terminal update ota");
                        }
                    }
                }
            }
        });
    }
*/

    private void showUpdateTerminalDialog(Activity activity) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    dialog = new Dialog(activity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_progressbar_dialog);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    SpinKitView spinKitView = dialog.findViewById(R.id.spin_kit);
                    percentage = dialog.findViewById(R.id.tvPercentage);
                    Sprite drawable = new FadingCircle();
                    spinKitView.setIndeterminateDrawable(drawable);
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getUserAuthToken() {
        String url = Constant.BASE_URL + "/api/getAuthenticateData";
        JSONObject obj = new JSONObject();
        try {
            obj.put("encryptedData", Constant.encryptedData);
            obj.put("retailerUserName", Constant.loginID);
            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");

                                if (status.equalsIgnoreCase("success")) {
                                    String userName = obj.getString("username");
                                    String userToken = obj.getString("usertoken");
                                    Constant.token = userToken;
                                } else {
                                    showAlert("Error");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                showAlert("Invalid Encrypted Data");
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            SweetDialogUtils.showMsg(BluetoothConnectActivity.this, "Please try again");
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}