package com.csc.firmware.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.csc.firmware.R;

import java.util.ArrayList;


public class BluetoothListAdapter extends BaseAdapter {

    private Context mContext;

    private ArrayList<BluetoothItem> devices;

    public BluetoothListAdapter(Context context) {
        this.mContext = context;
        this.devices = new ArrayList<BluetoothItem>();
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void clear() {
        devices.clear();
    }

    public void addItem(BluetoothItem item) {
        for (BluetoothItem ditem : devices) {
            if (ditem.getAddress().equals(item.getAddress())) {
                if (item.getName().length() > 0) {
                    ditem.setName(item.getName());
                }
                return;
            }
        }
        devices.add(item);
    }

    //Toggle selected items
    public void setSelected(int index) {
        int nSize = devices.size();

        for (int i = 0; i < nSize; ++i) {
            devices.get(i).setSelected(index == i);
        }

        notifyDataSetChanged();
    }

    public int getSelected() {
        int nSize = devices.size();

        for (int i = 0; i < nSize; ++i) {
            if (devices.get(i).isSelected()) {
                return i;
            }
        }

        return -1;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        BluetoothItem device = devices.get(position);

        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.bluetoothlist_item, parent, false);
        } else {
            view = convertView;
        }
        TextView tv = (TextView) view.findViewById(R.id.name);// display text
        ImageView imageView = (ImageView) view.findViewById(R.id.btImg);
        if (devices.size() > 0) {
                tv.setText(device.getName());
                //tv.setText("[" + device.getAddress() + "]" + device.getName());
        }
        return view;
    }
}
