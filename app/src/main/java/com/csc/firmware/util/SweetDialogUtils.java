package com.csc.firmware.util;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.csc.firmware.R;
import com.csc.firmware.custom_progressbar.FadingCircle;
import com.csc.firmware.custom_progressbar.SpinKitView;
import com.csc.firmware.custom_progressbar.Sprite;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class SweetDialogUtils {
    private static SweetAlertDialog pDialog;
    private static Dialog customDialog;
    private static TextView tvMsg;

    public static void showNormal(final Activity context, final String contextText) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                        .setContentText(contextText)
                        .show();
            }
        });

    }

    public static void showSuccess(final Activity context, final String contextText) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setContentText(contextText)
                        .show();
            }
        });

    }

    public static void showError(final Activity context, final String contextText) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setContentText(contextText)
                        .show();
            }
        });

    }

    public static void showProgress(final Activity context, final String text, final boolean cancelable) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                pDialog.setTitleText(text);
                pDialog.setCancelable(cancelable);
                pDialog.show();
            }
        });
    }

    public static  void cancelProgress(Activity context){
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.dismiss();
            }
        });
    }


    public static void changeAlertType(Activity activity, final String text, final int type) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (pDialog != null) {
                    pDialog.changeAlertType(type);
                    pDialog.setTitleText(text);
                }
                //pDialog = null;
            }
        });
    }

    public static void setProgressText(Activity activity, final String text) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(pDialog!=null)
                    pDialog.setTitleText(text);
            }
        });
    }

    public static void showMsg(Activity activity, final String text) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public static void showCustomDialogProgress(Activity activity,String msg,final boolean cancelable){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                customDialog = new Dialog(activity);
                customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                customDialog.setCancelable(cancelable);
                customDialog.setContentView(R.layout.layout_custon_progressbar_dialog);
                customDialog.getWindow().setBackgroundDrawableResource(R.drawable.custom_dialog_background);
                SpinKitView spinKitView = customDialog.findViewById(R.id.spin_kit_progressbar);
                tvMsg=customDialog.findViewById(R.id.tvMsg);
                tvMsg.setText(msg);
                Sprite drawable = new FadingCircle();
                spinKitView.setIndeterminateDrawable(drawable);
                customDialog.show();
            }
        });
    }

    public static void showCustomDialog(Activity activity, String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(customDialog!=null){
                        tvMsg.setText(msg);
                    }
//                    customDialog = new Dialog(activity);
//                    customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    customDialog.setCancelable(false);
//                    customDialog.setContentView(R.layout.layout_custon_progressbar_dialog);
//                    customDialog.getWindow().setBackgroundDrawableResource(R.drawable.custom_dialog_background);
//                    SpinKitView spinKitView = customDialog.findViewById(R.id.spin_kit_progressbar);
//                    TextView tvMsg=customDialog.findViewById(R.id.tvMsg);
//                    tvMsg.setText(msg);
//                    Sprite drawable = new FadingCircle();
//                    spinKitView.setIndeterminateDrawable(drawable);
//                    customDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public static void cancelCustomDialog(Activity activity){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(customDialog!=null) {
                    customDialog.dismiss();
                }
            }
        });
    }
}
